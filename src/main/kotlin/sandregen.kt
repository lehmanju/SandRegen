package de.devpi
import org.bukkit.Location
import org.bukkit.Material
import org.bukkit.entity.EntityType
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.entity.EntityChangeBlockEvent
import org.bukkit.plugin.java.JavaPlugin

class SandRegen() : JavaPlugin(), Listener
{
    override fun onEnable() {
        reloadConfig()
        config.addDefault("cobblestoneToGravel", true)
        config.addDefault("gravelToSand", true)
        config.options().copyDefaults(true)
        saveConfig()
        if (config.getBoolean("cobblestoneToGravel") || config.getBoolean("gravelToSand"))
            server.pluginManager.registerEvents(this, this)
    }

    @EventHandler
    fun onFallingBlock(event : EntityChangeBlockEvent)
    {
        if (event.entityType == EntityType.FALLING_BLOCK && event.to == Material.ANVIL)
        {
            val anvilLocation = event.block.location
            val location = Location(anvilLocation.world, anvilLocation.x,anvilLocation.y - 0.6, anvilLocation.z)
            val block = anvilLocation.world!!.getBlockAt(location)
            if (block.type == Material.COBBLESTONE && config.getBoolean("cobblestoneToGravel"))
                block.setType(Material.GRAVEL, true)
            else if (block.type == Material.GRAVEL  && config.getBoolean("gravelToSand"))
                block.setType(Material.SAND, true)
        }
    }
}
